#to prevent server error 
echo "nameserver 8.8.8.8" >> /etc/resolv.conf
echo "nameserver 8.8.4.4" >> /etc/resolv.conf

# install ansible (http://docs.ansible.com/intro_installation.html)

sudo apt update  --yes
sudo apt install software-properties-common  --yes
sudo apt-add-repository --yes --update ppa:ansible/ansible
sudo apt update --yes
sudo apt install ansible --yes

if [ ! -f "/home/vagrant/.ssh/id_rsa" ]; then
  ssh-keygen -t rsa -N "" -f /home/vagrant/.ssh/id_rsa
fi
cp /home/vagrant/.ssh/id_rsa.pub /vagrant/control.pub
cat << 'SSHEOF' > /home/vagrant/.ssh/config
 Host *
  StrictHostKeyChecking no
  UserKnownHostsFile=/dev/null
SSHEOF
chown -R vagrant:vagrant /home/vagrant/.ssh/ 
		 
		 
		 
		 
		 
		 
		 
#sudo mkdir /vagrant/.ssh
#sudo ssh-keygen -t rsa -f "/vagrant/.ssh/id_rsa" -P ""
#cat /vagrant/.ssh/id_rsa.pub >> /home/vagrant/.ssh/authorized_keys


#sudo ssh-copy-id -i /vagrant/.ssh/id_rsa app
#sudo ssh-copy-id -i /vagrant/.ssh/id_rsa app
#sudo ssh-copy-id web
#sudo ssh-copy-id db

#echo "ssh-add" >> /home/vagrant/.bash_profile




# copy examples into /home/vagrant (from inside the mgmt node)
#cp -a /vagrant/examples/* /home/vagrant
#chown -R vagrant:vagrant /home/vagrant

# configure hosts file for our internal network defined by Vagrantfile
#cat >> /etc/hosts

# vagrant environment nodes
#10.0.15.10  mgmt
#10.0.15.11  lb
#10.0.15.21  web1
#10.0.15.22  web2
#10.0.15.23  web3
#10.0.15.24  web4
#10.0.15.25  web5
#10.0.15.26  web6
#10.0.15.27  web7
#10.0.15.28  web8
#10.0.15.29  web9